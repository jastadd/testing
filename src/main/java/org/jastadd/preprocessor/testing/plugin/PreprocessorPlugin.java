package org.jastadd.preprocessor.testing.plugin;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.tasks.TaskCollection;

import java.util.Set;

/**
 * Plugin for preprocessor.testing.
 *
 * @author rschoene - Initial contribution
 */
public class PreprocessorPlugin implements Plugin<Project> {

  private Task testTask;

  @Override
  public void apply(Project project) {
    CompilerLocationExtension extension = project.getExtensions().create(
        "preprocessorTesting",
        CompilerLocationExtension.class,
        project);

    Set<Task> tasks = project.getTasksByName("compileTestJava", false);
    // there should be only one task "compileTestJava"
    testTask = tasks.iterator().next();

    // set compiler locations (if set)
    project.afterEvaluate(p -> {
      RelastTest.setCompilerLocation(extension.getRelastCompilerLocation().getOrNull());
      RagConnectTest.setRagconnectCompilerLocation(extension.getRagconnectCompilerLocation().getOrNull());
    });

    // setup tasks
    TaskCollection<RelastTest> relastTests = project.getTasks().withType(RelastTest.class);
    relastTests.whenTaskAdded(this::setupRelastTest);
    TaskCollection<RagConnectTest> ragconnectTests = project.getTasks().withType(RagConnectTest.class);
    ragconnectTests.whenTaskAdded(this::setupRagconnectTest);
  }

  private void setupRelastTest(RelastTest relastTest) {
    testTask.dependsOn(relastTest);
    relastTest.setGroup("verification");
    relastTest.setDescription("Runs a relast test");
  }

  private void setupRagconnectTest(RagConnectTest ragConnectTest) {
    testTask.dependsOn(ragConnectTest);
    ragConnectTest.setGroup("verification");
    ragConnectTest.setDescription("Runs a ragconnect test");
  }
}
