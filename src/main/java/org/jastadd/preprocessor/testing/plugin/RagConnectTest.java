package org.jastadd.preprocessor.testing.plugin;

import groovy.lang.Closure;
import org.gradle.api.Action;
import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.tasks.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static groovy.lang.Closure.DELEGATE_FIRST;

/**
 * RagConnect Test Task.
 *
 * @author rschoene - Initial contribution
 */
public abstract class RagConnectTest extends RelastTest {

  @Nested
  abstract RagConnectConfiguration getRagconnect();

  private static String ragconnectCompilerLocation;

  public static void setRagconnectCompilerLocation(String ragconnectCompilerLocation) {
    RagConnectTest.ragconnectCompilerLocation = ragconnectCompilerLocation;
  }

  @SuppressWarnings("unused")
  public void ragconnect(Closure<?> c) {
    c.setResolveStrategy(DELEGATE_FIRST);
    c.setDelegate(getRagconnect());
    c.call();
  }

  @SuppressWarnings("unused")
  public void ragconnect(Action<? super RagConnectConfiguration> action) {
    action.execute(getRagconnect());
  }

  @OutputFiles
  public List<File> getGrammarFilesGeneratedByRagconnect() {
    // files are RagConnect.relast and <INPUT_GRAMMAR(s)>.relast
    List<File> result = new ArrayList<>();
    result.add(resolveOutputDir("RagConnect.relast"));
    for (File inputFile : getRagconnect().getInputFiles()) {
      String filename = inputFile.getName();
      if (Utils.fileExtension(filename).equals("relast")) {
        result.add(resolveOutputDir(filename));
      }
    }
    return result;
  }

  @OutputFiles
  public List<File> getAspectFilesGeneratedByRagconnect() {
    // file is only RagConnect.jadd
    List<File> result = new ArrayList<>();
    result.add(resolveOutputDir("RagConnect.jadd"));
    return result;
  }

  private File resolveOutputDir(String s) {
    return Paths.get(getRagconnect().getOutputDir(), s).toFile();
  }

  @TaskAction
  void taskAction() throws IOException {
    runTest();
  }

  protected void runTest() throws IOException {
    Project project = getProject();

    // delete generated files
    project.delete(deleteSpec -> {
      deleteSpec.delete(getGrammarFilesGeneratedByRagconnect());
      deleteSpec.delete(getAspectFilesGeneratedByRagconnect());
    });

    // run ragconnect first
    project.getPlugins().withType(JavaPlugin.class, javaPlugin -> {
      SourceSetContainer sourceSets = (SourceSetContainer) project.getProperties().get("sourceSets");
      FileCollection runtimeClasspath = sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME).getRuntimeClasspath();
      project.javaexec(javaExecSpec -> {
        List<Object> args = new ArrayList<>();
        javaExecSpec.setClasspath(runtimeClasspath);
        if (ragconnectCompilerLocation != null) {
          javaExecSpec.setMain("-jar");
          args.add(ragconnectCompilerLocation);
        } else {
          javaExecSpec.setMain("org.jastadd.ragconnect.compiler.Compiler");
        }
        args.add("--o=" + getRagconnect().getOutputDir());
        args.add("--rootNode=" + getRagconnect().getRootNode());
        if (valueOf(getRagconnect().isLogReads(), false)) {
          args.add("--logReads");
        }
        if (valueOf(getRagconnect().isLogWrites(), false)) {
          args.add("--logWrites");
        }
        if (valueOf(getRagconnect().isLogIncremental(), false)) {
          args.add("--logIncremental");
        }
        if (valueOf(getRagconnect().isVerbose(), false)) {
          args.add("--verbose");
        }
        if (getRagconnect().getProtocols() != null && !getRagconnect().getProtocols().isEmpty()) {
          args.add("--protocols=" + String.join(",", getRagconnect().getProtocols()));
        }
        if (getJastadd().getJastAddList() != null) {
          args.add("--List=" + getJastadd().getJastAddList());
        }
        args.addAll(getRagconnect().getExtraOptionsOrDefault());
        args.addAll(getJastadd().getExtraOptionsOrDefault());
        args.addAll(getRagconnect().getInputFiles());
        javaExecSpec.args(args);
      }).assertNormalExitValue();
    });


    // now run relast + jastadd
    getRelast().setInputFiles(getGrammarFilesGeneratedByRagconnect());
    if (getJastadd().getInputFiles() == null) {
      getJastadd().setInputFiles(new ArrayList<>());
    }
    for (File file : getAspectFilesGeneratedByRagconnect()) {
      if (getJastadd().getInputFiles().contains(file)) {
        getLogger().warn("Input file to JastAdd already known: {} - Remove from jastadd.inputFiles!", file);
      } else {
        getJastadd().getInputFiles().add(file);
      }
    }
    super.runTest();
  }
}
