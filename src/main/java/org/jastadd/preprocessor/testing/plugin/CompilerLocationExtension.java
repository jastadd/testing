package org.jastadd.preprocessor.testing.plugin;

import org.gradle.api.Project;
import org.gradle.api.provider.Property;

/**
 * TODO: Add description.
 *
 * @author rschoene - Initial contribution
 */
public class CompilerLocationExtension {
  public Property<String> relastCompilerLocation;
  public Property<String> ragconnectCompilerLocation;

  public CompilerLocationExtension(Project project) {
    relastCompilerLocation = project.getObjects().property(String.class);
    ragconnectCompilerLocation = project.getObjects().property(String.class);
  }

  public Property<String> getRelastCompilerLocation() {
    return relastCompilerLocation;
  }

  public Property<String> getRagconnectCompilerLocation() {
    return ragconnectCompilerLocation;
  }
}
