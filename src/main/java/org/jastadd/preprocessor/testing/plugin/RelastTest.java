package org.jastadd.preprocessor.testing.plugin;

import groovy.lang.Closure;
import org.gradle.api.Action;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.tasks.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static groovy.lang.Closure.DELEGATE_FIRST;

/**
 * RelAst Test Task
 *
 * @author rschoene - Initial contribution
 */
public abstract class RelastTest extends DefaultTask {
  // configuration from plugin
  private static String compilerLocation;

  @Nested
  abstract RelastConfiguration getRelast();

  @Nested
  abstract JastAddConfiguration getJastadd();

  public static void setCompilerLocation(String compilerLocation) {
    RelastTest.compilerLocation = compilerLocation;
  }

  private static final String[] genSuffixes = {".ast", ".jadd", "RefResolver.jadd", "ResolverStubs.jrag", "Serializer.jadd"};

  private Path pathToAbsoluteProject(String filename) {
    return Paths.get(getProject().getProjectDir().getAbsolutePath(), filename);
  }

  @SuppressWarnings("unused")
  public void relast(Closure<?> c) {
    c.setResolveStrategy(DELEGATE_FIRST);
    c.setDelegate(getRelast());
    c.call();
  }

  @SuppressWarnings("unused")
  public void relast(Action<? super RelastConfiguration> action) {
    action.execute(getRelast());
  }

  @SuppressWarnings("unused")
  public void jastadd(Closure<?> c) {
    c.setResolveStrategy(DELEGATE_FIRST);
    c.setDelegate(getJastadd());
    c.call();
  }

  @SuppressWarnings("unused")
  public void jastadd(Action<? super JastAddConfiguration> action) {
    action.execute(getJastadd());
  }

  @TaskAction
  void taskAction() throws IOException {
    runTest();
  }

  protected boolean valueOf(Boolean b, boolean defaultValue) {
    return b != null ? b : defaultValue;
  }

  @Internal
  protected boolean isVerbose() {
    return getRelast().isVerbose() != null && getRelast().isVerbose();
  }

  protected void runTest() throws IOException {
    Project project = getProject();
    if (isVerbose()) {
      System.out.println("Running relast test in " + project.getDisplayName());
      System.out.println("relast files: " + getRelast().getInputFiles());
      System.out.println("Deleting files");
    }
    // first, delete generated files
    List<Path> genFiles = new ArrayList<>();
    for (String suffix : genSuffixes) {
      genFiles.add(pathToAbsoluteProject(getRelast().getGrammarName() + suffix));
    }
    if (isVerbose()) {
      System.out.println("gen files: " + genFiles);
    }
    project.delete(deleteSpec -> {
      deleteSpec.delete(genFiles);
//      deleteSpec.delete(Paths.get(jastadd.outputDir, jastadd.packageName));
      deleteSpec.delete(getJastadd().getOutputDirOrDefault().toPath().resolve(getJastadd().getPackageName()));
    });
    // create output directories, if not existing
    createDirectory(pathToAbsoluteProject(getJastadd().getOutputDirOrDefault().getPath()));
    createDirectory(pathToAbsoluteProject(getRelast().getGrammarName()).getParent());
    if (isVerbose()) {
      System.out.println("Pre processing, running relast");
    }
    // then, run relast pre-processing
    project.getPlugins().withType(JavaPlugin.class, javaPlugin -> {
      SourceSetContainer sourceSets = (SourceSetContainer) project.getProperties().get("sourceSets");
      FileCollection runtimeClasspath = sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME).getRuntimeClasspath();
      project.javaexec(javaExecSpec -> {
        List<String> args = new ArrayList<>();
        javaExecSpec.setClasspath(runtimeClasspath);
        if (RelastTest.compilerLocation != null) {
          javaExecSpec.setMain("-jar");
          args.add(RelastTest.compilerLocation);
        } else {
          javaExecSpec.setMain("org.jastadd.relast.compiler.Compiler");
        }
        for (File file : getRelast().getInputFiles()) {
          args.add(file.getAbsolutePath());
        }
        args.add("--quiet");
        if (!valueOf(getRelast().isNoWriteToFile(), false)) {
          args.add("--file");
        }
        if (valueOf(getRelast().isUseJastAddNames(), true)) {
          args.add("--useJastAddNames");
        }
        if (!valueOf(getRelast().isNoResolverHelper(), false)) {
          args.add("--resolverHelper");
        }
        if (getJastadd().getJastAddList() != null) {
          args.add("--jastAddList=" + getJastadd().getJastAddList());
        }
        if (getRelast().getListClass() != null) {
          args.add("--listClass=" + getRelast().getListClass());
        }
        if (getRelast().getSerializer() != null) {
          args.add("--serializer=" + getRelast().getSerializer());
        }
        args.add("--grammarName=" + pathToAbsoluteProject(getRelast().getGrammarName()));
        args.addAll(getRelast().getExtraOptionsOrDefault());
        if (isVerbose()) {
          System.out.println("Start relast with args: " + args + " and main: " + javaExecSpec.getMainClass().getOrNull());
        }
        javaExecSpec.args(args);
      }).assertNormalExitValue();
    });
    if (valueOf(getJastadd().isSkipRun(), false)) {
      if (isVerbose()) {
        System.out.println("Skipping run of JastAdd");
      }
    } else {
      if (isVerbose()) {
        System.out.println("Compile with JastAdd");
      }
      // check which files were actually generated
      genFiles.removeIf(this::verboseFileNotExists);
      // finally, compile generated files
      project.getPlugins().withType(JavaPlugin.class, javaPlugin -> {
        SourceSetContainer sourceSets = (SourceSetContainer) project.getProperties().get("sourceSets");
        FileCollection runtimeClasspath = sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME).getRuntimeClasspath();
        project.javaexec(javaExecSpec -> {
          javaExecSpec.setClasspath(runtimeClasspath);
          javaExecSpec.setMain("org.jastadd.JastAdd");
          List<Object> args = new ArrayList<>();
          args.add("--o=" + pathToAbsoluteProject(getJastadd().getOutputDirOrDefault().getPath()));
          args.add("--package=" + getJastadd().getPackageName());
          if (getJastadd().getJastAddList() != null) {
            args.add("--List=" + getJastadd().getJastAddList());
          }
          args.addAll(getJastadd().getExtraOptionsOrDefault());
          args.addAll(genFiles);
          args.addAll(getJastadd().getInputFiles());
          if (isVerbose()) {
            System.out.println("Start JastAdd with args: " + args);
          }
          javaExecSpec.args(args);
        });
      });
    }
  }

  private boolean verboseFileNotExists(Path path) {
    boolean fileDoesNotExist = !Files.exists(path);
    if (fileDoesNotExist && isVerbose()) {
      System.out.println("Do not include " + path);
    }
    return fileDoesNotExist;
  }

  private void createDirectory(Path path) throws IOException {
    if (Files.exists(path) && Files.isDirectory(path)) {
      return;
    }
    if (isVerbose()) {
      System.out.println("Creating " + path.toAbsolutePath());
    }
    try {
      Files.createDirectories(path);
    } catch (FileAlreadyExistsException e) {
      System.err.println("Skipping creation of already existing " + path);
    } catch (IOException e) {
      System.err.println("Could not create output directory " + path);
      throw e;
    }
  }

}
