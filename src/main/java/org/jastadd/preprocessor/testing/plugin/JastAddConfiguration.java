package org.jastadd.preprocessor.testing.plugin;

import org.gradle.api.tasks.*;
import org.jastadd.preprocessor.testing.doc.Description;

import java.io.File;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

/**
 * Configuration options for JastAdd.
 *
 * @author rschoene - Initial contribution
 */
public interface JastAddConfiguration {

  @Description(value = "Package name of AST files", correspondingParameter = "--package")
  @Input
  String getPackageName();
  void setPackageName(String packageName);

  @Description(value = "Name of the List class", correspondingParameter = "--List")
  @Optional
  @Input
  String getJastAddList();
  void setJastAddList(String jastAddList);

  @Description(value = "Do not run JastAdd")
  @Optional
  @Input
  Boolean isSkipRun();
  void setSkipRun(Boolean skipRun);

  @Description(value = "Add more options not directly supported")
  @Internal
  List<String> getExtraOptions();
  void setExtraOptions(List<String> extraOptions);

  @Description(value = "internal", skip = true)
  @Input
  default List<String> getExtraOptionsOrDefault() {
    return getExtraOptions() != null ? getExtraOptions() : Collections.emptyList();
  }

  @Description(value = "Add new input files to be processed")
  @Optional
  @InputFiles
  List<File> getInputFiles();
  void setInputFiles(List<File> inputFiles);

  @Description(value = "Output directory (defaults to `src/test/java-gen`)", correspondingParameter = "--o")
  @Optional
  @OutputDirectory
  File getOutputDir();
  void setOutputDir(File outputDir);

  @Description(value = "internal", skip = true)
  @OutputDirectory
  default File getOutputDirOrDefault() {
    return getOutputDir() != null ? getOutputDir() : Paths.get("src", "test", "java-gen").toFile();
  }

  @Description(value = "internal", skip = true)
  @OutputDirectory
  default File getPackageOutputDir() {
    if (isSkipRun() != null && isSkipRun()) {
      return new File(".");
    }
    return getOutputDirOrDefault().toPath().resolve(getPackageName().replace('.', File.separatorChar)).toFile();
  }

}
