package org.jastadd.preprocessor.testing.plugin;

import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.Optional;
import org.jastadd.preprocessor.testing.doc.Description;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Configuration options for RagConnect.
 *
 * @author rschoene - Initial contribution
 */
public interface RagConnectConfiguration {

  @Description(value = "Sets the root node", correspondingParameter = "--rootNode")
  @Input
  String getRootNode();
  void setRootNode(String rootNode);

  @Description(value = "Sets the output directory", correspondingParameter = "--o")
  @Input
  String getOutputDir();
  void setOutputDir(String outputDir);

  @Description(value = "Add new input files to be processed")
  @InputFiles
  List<File> getInputFiles();
  void setInputFiles(List<File> inputFiles);

  @Description(value = "Log read endpoints", correspondingParameter = "--logReads")
  @Optional
  @Input
  Boolean isLogReads();
  void setLogReads(Boolean logReads);

  @Description(value = "Log write endpoints", correspondingParameter = "--logWrites")
  @Optional
  @Input
  Boolean isLogWrites();
  void setLogWrites(Boolean logWrites);

  @Description(value = "Log incremental activities", correspondingParameter = "--logIncremental")
  @Optional
  @Input
  Boolean isLogIncremental();
  void setLogIncremental(Boolean logIncremental);

  @Description(value = "Be more verbose during compilation", correspondingParameter = "--verbose")
  @Optional
  @Input
  Boolean isVerbose();
  void setVerbose(Boolean verbose);

  @Description(value = "Set protocols to be used", correspondingParameter = "--protocols")
  @Optional
  @Input
  List<String> getProtocols();
  void setProtocols(List<String> protocols);

  @Description(value = "Add more options not directly supported")
  @Internal
  List<String> getExtraOptions();
  void setExtraOptions(List<String> extraOptions);

  @Description(value = "internal", skip = true)
  @Input
  default List<String> getExtraOptionsOrDefault() {
    return getExtraOptions() != null ? getExtraOptions() : Collections.emptyList();
  }
}
