package org.jastadd.preprocessor.testing.plugin;

import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.Optional;
import org.jastadd.preprocessor.testing.doc.Description;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Configuration options for Relast.
 *
 * @author rschoene - Initial contribution
 */
public interface RelastConfiguration {

  @Description(value = "Use JastAdd like API for relations", correspondingParameter = "--useJastAddNames")
  @Optional
  @Input
  Boolean isUseJastAddNames();
  void setUseJastAddNames(Boolean useJastAddNames);

  @Description(value = "Name of resulting grammar (can be a path)", correspondingParameter = "--grammarName")
  @Input
  String getGrammarName();
  void setGrammarName(String grammarName);

  @Description(value = "Add new input files to be processed")
  @Optional
  @InputFiles
  List<File> getInputFiles();
  void setInputFiles(List<File> inputFiles);

  @Description(value = "Do not write any files", correspondingParameter = "--file")
  @Optional
  @Input
  Boolean isNoWriteToFile();
  void setNoWriteToFile(Boolean noWriteToFile);

  @Description(value = "Do not generate resolverHelper", correspondingParameter = "--resolverHelper")
  @Optional
  @Input
  Boolean isNoResolverHelper();
  void setNoResolverHelper(Boolean noResolverHelper);

  @Description(value = "Class to be used for relations", correspondingParameter = "--listClass")
  @Optional
  @Input
  String getListClass();
  void setListClass(String listClass);

  @Description(value = "Activate serialization, and set its implementation", correspondingParameter = "--serializer")
  @Internal
  String getSerializer();
  void setSerializer(String serializer);

  @Description(value = "Be more verbose during compilation")
  @Optional
  @Input
  Boolean isVerbose();
  void setVerbose(Boolean verbose);

  @Description(value = "Add more options not directly supported")
  @Optional
  @Input
  List<String> getExtraOptions();
  void setExtraOptions(List<String> extraOptions);

  @Description(value = "internal", skip = true)
  @Input
  default List<String> getExtraOptionsOrDefault() {
    return getExtraOptions() != null ? getExtraOptions() : Collections.emptyList();
  }
}
