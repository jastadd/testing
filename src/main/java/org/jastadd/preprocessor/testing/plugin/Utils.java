package org.jastadd.preprocessor.testing.plugin;

/**
 * Utility methods.
 *
 * @author rschoene - Initial contribution
 */
public class Utils {

  public static String fileExtension(String filename) {
    int indexOfDot = filename.lastIndexOf('.');
    return indexOfDot == -1 ? filename : filename.substring(indexOfDot + 1);
  }
}
