package org.jastadd.preprocessor.testing.doc;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Description {
  String value();
  String correspondingParameter() default "";
  boolean skip() default false;
}
