package org.jastadd.preprocessor.testing.doc;

import org.gradle.api.tasks.*;
import org.jastadd.preprocessor.testing.plugin.JastAddConfiguration;
import org.jastadd.preprocessor.testing.plugin.RagConnectConfiguration;
import org.jastadd.preprocessor.testing.plugin.RelastConfiguration;

import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class DocumentationCreator {
  private static final String HEADER = "# Configuration options\n\n";
  private static final List<Map.Entry<String, Class<?>>> entitiesToBeDocumented = new ArrayList<Map.Entry<String, Class<?>>>() {{
    add(makeMapEntry("RagConnect Configuration", RagConnectConfiguration.class));
    add(makeMapEntry("Relast Configuration", RelastConfiguration.class));
    add(makeMapEntry("JastAdd Configuration", JastAddConfiguration.class));
  }};

  private static Map.Entry<String, Class<?>> makeMapEntry(String configurationName, Class<?> configurationClass) {
    return new Map.Entry<String, Class<?>>() {
      @Override
      public String getKey() {
        return configurationName;
      }

      @Override
      public Class<?> getValue() {
        return configurationClass;
      }

      @Override
      public Class<?> setValue(Class<?> o) {
        throw new UnsupportedOperationException("setValue not allowed");
      }
    };
  }

  private static final String outputFile = "pages/docs/config.md";

  public static void main(String[] args) {
    new DocumentationCreator().run();
  }

  protected void run() {
    PriorityQueue<String[]> options = new PriorityQueue<>((e1, e2) -> {
      if (e1[1].equals("Yes")) {
        if (e2[1].equals("Yes")) {
          return e1[0].compareTo(e2[0]);
        }
        return -1;
      }
      if (e2[1].equals("Yes")) {
        return 1;
      }
      return e1[0].compareTo(e2[0]);
    });
    Path outputPath = Paths.get(outputFile);
    System.out.println("Writing to " + outputPath.toAbsolutePath());
    try(BufferedWriter writer = Files.newBufferedWriter(outputPath,
        StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
      writer.write(HEADER);
      for (Map.Entry<String, Class<?>> entry : entitiesToBeDocumented) {
        options.clear();
        writer.write("## " + entry.getKey() + "\n\n");
        writer.write("Name | Required? | Corresponding parameter | Description\n");
        writer.write("--- | --- | --- | ---\n");
        for (Method method : entry.getValue().getMethods()) {
          if (method.isAnnotationPresent(Input.class) ||
              method.isAnnotationPresent(InputFiles.class) ||
              method.isAnnotationPresent(OutputDirectory.class) ||
              method.isAnnotationPresent(Description.class)) {
            String name = nameOfOption(method.getName());
            String required = method.isAnnotationPresent(Optional.class) ? "No" : "Yes";
            final String parameter, description;
            if (method.isAnnotationPresent(Description.class)) {
              Description descriptionAnnotation = method.getAnnotation(Description.class);
              if (descriptionAnnotation.skip()) {
                continue;
              }
              if (descriptionAnnotation.correspondingParameter().trim().isEmpty()) {
                parameter = "";
              } else {
                parameter = "`" + descriptionAnnotation.correspondingParameter() + "`";
              }
              description = descriptionAnnotation.value();
            } else {
              parameter = "";
              description = "_not provided_";
            }
            options.add(new String[]{name, required, parameter, description});
          }
        }
        while(!options.isEmpty()) {
          String[] option = options.poll();
          String toWrite = String.format("%s | %s | %s | %s%n", (Object[]) option);
          writer.write(toWrite);
        }
        writer.write("\n\n");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private String nameOfOption(String methodName) {
    if (methodName == null || methodName.isEmpty()) { return methodName; }
    final String s;
    if (methodName.startsWith("get")) {
      s = methodName.substring(3);
    } else if (methodName.startsWith("is")) {
      s = methodName.substring(2);
    } else {
      throw new RuntimeException("Unknown prefix in method " + methodName);
    }
    return Character.toLowerCase(s.charAt(0)) + s.substring(1);
  }
}
