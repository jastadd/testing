buildGradle = '../build.gradle'


def get_version():
    with open(buildGradle) as fd:
        for line in fd.readlines():
            # look for line `version 'Major.Minor.Patch'`
            if line.startswith('version '):
                return line[9:-2]
    return '?'


def define_env(env):
    """
    This is the hook for defining variables, macros and filters

    - variables: the dictionary that contains the environment variables
    - macro: a decorator function, to declare a macro.
    """
    env.conf['site_name'] = 'Gradle Plugin RelastTest ' + get_version()


if __name__ == '__main__':
    print(get_version())
