# Gradle Plugin for JastAdd preprocessors

This [Gradle plugin](https://git-st.inf.tu-dresden.de/jastadd/testing) is providing means to write Gradle tasks to invoke the two preprocessors [Relast]() and [RagConnect](https://jastadd.pages.st.inf.tu-dresden.de/ragconnect) as well as [JastAdd](http://jastadd.org/) itself.

To use this plugin, add the following to your `build.gradle`:

```groovy
buildscript {
    repositories {
        mavenCentral()
        maven {
            name "gitlab-maven"
            url "https://git-st.inf.tu-dresden.de/api/v4/groups/jastadd/-/packages/maven"
        }
    }
    dependencies {
        classpath 'org.jastadd.preprocessor:testing:0.2.8'
    }
}

import org.jastadd.preprocessor.testing.plugin.PreprocessorPlugin
import org.jastadd.preprocessor.testing.plugin.RagConnectTest

apply plugin: PreprocessorPlugin
```

A typical usage (taken from tests used for RagConnect) looks as follows:

```groovy
task compileTreeIncremental(type: RagConnectTest) {
    ragconnect {
        outputDir = file('src/test/02-after-ragconnect/treeInc')
        inputFiles = [file('src/test/01-input/tree/Test.relast'),
                      file('src/test/01-input/tree/Test.connect')]
        rootNode = 'Root'
        logWrites = true
    }
    relast {
        useJastAddNames = true
        grammarName = 'src/test/03-after-relast/treeInc/treeInc'
        serializer = 'jackson'
    }
    jastadd {
        jastAddList = 'JastAddList'
        packageName = 'treeInc.ast'
        inputFiles = [file('src/test/01-input/tree/Test.jadd')]
        extraOptions = ['--tracing=cache,flush',
                        '--incremental=param',
                        '--cache=all',
                        '--rewrite=cnta',
                        '--flush=full']
    }
}
```

Available options are listed at [configuration](config).
Notably, all options passed to JastAdd are also passed to the preprocessors, and generated files are handled correctly, e.g., generated aspects are passed to JastAdd automatically.

The compilers to be used can be configured as follows:

```groovy
preprocessorTesting {
    //noinspection GroovyAssignabilityCheck
    relastCompilerLocation = '../libs/relast.jar'
    ragconnectCompilerLocation = '../libs/ragconnect.jar'
}
```

By default, the compilers are expected to be available in the classpath.
